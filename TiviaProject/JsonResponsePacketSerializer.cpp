#include "JsonResponsePacketSerializer.h"


/*
The function creates the server response message using the given code and data itself
Input: the response message code and the data to send in the server response
Output: the server response as a string
*/
std::string JsonResponsePacketSerializer::createResponse(ResponseMessageCode responseCode, std::string responseMsg)
{
	std::string code = convertIntToByte((int)responseCode);  //converts the code of the response to 4 bytes
	std::string length = convertIntToByte(responseMsg.length());  //converts the length of the message to 4 bytes
	
	//creates the whole response message: takes the first byte of the code, adds to it the 4 bytes of the data length and adds the data itself
	std::string message = code[code.length() - 1] + length + responseMsg;
	return message;
}


/*
The function converts the given number to a 4 byte vector
Input: a number to convert to 4 bytes
Output: a string of the converted 4 bytes
*/
std::string JsonResponsePacketSerializer::convertIntToByte(int number)
{
	//converts the int to a vector of 4 bytes
	unsigned char* bytePointer = reinterpret_cast<unsigned char*>(&number);
	std::vector<unsigned char> bytes(bytePointer, bytePointer + sizeof(int));

	std::reverse(bytes.begin(), bytes.end());

	return std::string(bytes.begin(), bytes.end());
}


/*
Each serializeResponse function gets a different struct which symbolizes different server response messages 
and converts it to a vector of chars to send to the socket
*/
Buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
	Json jsonResponse;
	jsonResponse["Status"] = response.status;

	Buffer fullResponse = createResponse(ResponseMessageCode::LoginResponseCode, jsonResponse.dump());
	return fullResponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
	Json jsonResponse;
	jsonResponse["Status"] = response.status;

	Buffer fullResponse = createResponse(ResponseMessageCode::SignupResponseCode, jsonResponse.dump());
	return fullResponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
	Json jsonResponse;
	jsonResponse["Message"] = response.error;

	Buffer fullResponse = createResponse(ResponseMessageCode::ErrorResponseCode, jsonResponse.dump());
	return fullResponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
	Json jsonResponse;
	jsonResponse["Status"] = response.status;

	Buffer fullReponse = createResponse(ResponseMessageCode::LogoutResponseCode, jsonResponse);
	return fullReponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
	Json jsonResponse;
	jsonResponse["Status"] = response.status;

	//runs through the whole vector of rooms and converts it to string with all the rooms and inserts it to the json
	std::string allRooms = "";
	for (auto currRoom : response.rooms)
	{
		allRooms += currRoom.toString();
	}
	jsonResponse["Rooms"] = allRooms;

	Buffer fullReponse = createResponse(ResponseMessageCode::GetRoomsResponseCode, jsonResponse);
	return fullReponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	Json jsonResponse;

	//converts the vector of all players to a string with all the players and inserts it to the json
	std::ostringstream allPlayers;
	if (!response.players.empty())
	{
		std::copy(response.players.begin(), response.players.end() - 1, std::ostream_iterator<std::string>(allPlayers, ", "));
		allPlayers << response.players.back();
	}
	jsonResponse["Players"] = allPlayers.str();

	Buffer fullReponse = createResponse(ResponseMessageCode::GetPlayersInRoomResponseCode, jsonResponse);
	return fullReponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
	Json jsonResponse;
	jsonResponse["Status"] = response.status;

	Buffer fullReponse = createResponse(ResponseMessageCode::JoinRoomResponseCode, jsonResponse);
	return fullReponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
	Json jsonResponse;
	jsonResponse["Status"] = response.status;

	Buffer fullReponse = createResponse(ResponseMessageCode::CreateRoomResponseCode, jsonResponse);
	return fullReponse;
}


Buffer JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse response)
{
	Json jsonResponse;
	jsonResponse["Status"] = response.status;

	//creates a sstream which converts the userstatistcs vector to a string and inserts it to the json
	std::ostringstream sstream;
	std::vector<std::string> userStatisticsVector = response.userStatistics.statistics;
	if (!userStatisticsVector.empty())
	{
		std::copy(userStatisticsVector.begin(), userStatisticsVector.end() - 1, std::ostream_iterator<std::string>(sstream, ", "));
		sstream << userStatisticsVector.back();
	}
	jsonResponse["UserStatistics"] = sstream.str();

	//clears the sstream and converts the highscores vector to a string and inserts it to the json
	sstream.clear();
	std::vector<std::string> highScoresVector = response.highScores.statistics;
	if (!highScoresVector.empty())
	{
		std::copy(highScoresVector.begin(), highScoresVector.end() - 1, std::ostream_iterator<std::string>(sstream, ", "));
		sstream << highScoresVector.back();
	}
	jsonResponse["HighScores"] = sstream.str();

	Buffer fullReponse = createResponse(ResponseMessageCode::GetStatisticsResponseCode, jsonResponse);
	return fullReponse;
}

