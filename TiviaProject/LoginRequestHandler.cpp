#include "LoginRequestHandler.h"


//cto'r
LoginRequestHandler::LoginRequestHandler(LoginManager& loginManager):
	m_loginManager(loginManager)
{
}


/*
The function checks if the given requestInfo struct id relevant: if the id is between the enum id types
Input: the request info to check
Output: true if the given request is relevant and false if not
*/
bool LoginRequestHandler::isRequestRelevant(RequestInfo info)
{
	return info.id >= RequestMessageCode::LoginRequestCode && info.id <= RequestMessageCode::SignupRequestCode;
}


/*
The function handles the given request from the struct and returns the result as a struct 
Input: a struct which represents the given request info
Output: a struct which represnets the response to the given request
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult result;

	switch (info.id)
	{
		case RequestMessageCode::LoginRequestCode:
		{
			result = this->login(info);

			break;
		}
		case RequestMessageCode::SignupRequestCode:
		{
			result = this->signup(info);

			break;
		}
		default:
		{
			throw std::exception("Invalid message id!");

			break;
		}
	}

	return result;
}


/*
The function receives a login request information, deserializes it, sends the login information to the login manager and creates a request result struct
Input: a struct which represents the given login request info
Output: a struct which represnets the response to the given login request
*/
RequestResult LoginRequestHandler::login(RequestInfo info)
{
	//deserializes the login request and sends the login information to the login manager
	LoginRequest request = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);
	m_loginManager.login(request.username, request.password);

	//creates the response struct and serializes it
	LoginResponse responseStruct;
	responseStruct.status = 1;  //temp
	Buffer responseBuffer = JsonResponsePacketSerializer::serializeResponse(responseStruct);

	return this->createRequestResult(responseBuffer, this);
}


/*
The function receives a signup request information, deserializes it, sends the signup information to the login manager and creates a request result struct
Input: a struct which represents the given signup request info
Output: a struct which represnets the response to the given signup request
*/
RequestResult LoginRequestHandler::signup(RequestInfo info)
{
	//deserializes the signup request and sends the signup information to the login manager
	SignupRequest request = JsonRequestPacketDeserializer::deserializeSignupRequest(info.buffer);
	m_loginManager.signup(request.username, request.password, request.email);

	//creates the response struct and serializes it
	SignupResponse responseStruct;
	responseStruct.status = 1;  //temp
	Buffer responseBuffer = JsonResponsePacketSerializer::serializeResponse(responseStruct);

	return this->createRequestResult(responseBuffer, this);
}

