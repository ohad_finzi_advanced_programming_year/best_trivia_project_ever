#pragma once

#include <vector>
#include <string>

#include "Room.h"


enum ResponseMessageCode
{
	ErrorResponseCode = 99,
	LoginResponseCode = 101,
	SignupResponseCode = 102,

	LogoutResponseCode = 103,
	GetRoomsResponseCode = 104,
	GetPlayersInRoomResponseCode = 105,
	JoinRoomResponseCode = 106,
	CreateRoomResponseCode = 107,
	GetStatisticsResponseCode = 108
};


struct LoginResponse
{
	unsigned int status;
};


struct SignupResponse
{
	unsigned int status;
};


struct ErrorResponse
{
	std::string error;
};


struct LogoutResponse
{
	unsigned int status;
};


struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};


struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
};


struct JoinRoomResponse
{
	unsigned int status;
};


struct CreateRoomResponse
{
	unsigned int status;
};


struct GetHighScoreResponse
{
	unsigned int status;
	std::vector<std::string> statistics;
};


struct GetPersonalStatsResponse
{
	unsigned int status;
	std::vector<std::string> statistics;
};


struct GetStatisticsResponse
{
	unsigned int status;
	GetHighScoreResponse highScores;
	GetPersonalStatsResponse userStatistics;
};

