#pragma once

#include "json.hpp"
using Json = nlohmann::json;

#include "Responses.h"

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>


typedef std::string Buffer;


class JsonResponsePacketSerializer
{
public:
	static Buffer serializeResponse(LoginResponse response);
	static Buffer serializeResponse(SignupResponse response);
	static Buffer serializeResponse(ErrorResponse response);

	static Buffer serializeResponse(LogoutResponse response);
	static Buffer serializeResponse(GetRoomsResponse response);
	static Buffer serializeResponse(GetPlayersInRoomResponse response);
	static Buffer serializeResponse(JoinRoomResponse response);
	static Buffer serializeResponse(CreateRoomResponse response);
	static Buffer serializeResponse(GetStatisticsResponse response);

private:
	static std::string createResponse(ResponseMessageCode responseCode, std::string responseMsg);
	static std::string convertIntToByte(int number);

};

