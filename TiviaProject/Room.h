#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "LoggedUser.h"

class IRequestHandler;


struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestions;
	unsigned int timePerQuestion;
	unsigned int isActive;

	std::string toString();
};


class Room
{
public:
	Room(RoomData data);
	Room() = default;

	void addUser(LoggedUser userToAdd);
	void removeUser(LoggedUser userToRemove);
	RoomData getRoomData();
	std::vector<std::string> getAllUsers();

private:
	//variabales
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;

};

