#include "Communicator.h"


//cto'r
Communicator::Communicator(RequestHandlerFactory& handlerFactory):
	m_handlerFactory(handlerFactory)
{
	this->bindAndListen();
}


/*
The function sends the given data to the given client socket
*/
void Communicator::sendData(SOCKET socket, std::string data)
{
	const char* sendData = data.c_str();
	if (send(socket, sendData, data.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}


/*
The function recieves the data inside the given client socket according to the given bytes
Input: the client socket to the recive data from and the number of bytes to recieve from it
Output: the data which got pulled from the given client socket as char array
*/
char* Communicator::getPartFromSocket(SOCKET sc, int bytesNum)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum];
	int res = recv(sc, data, bytesNum, 0);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	return data;
}


/*
The function gets a part from the given socket according to the given bytes num and converts it to int
Input: the client socket to the recive data from and the number of bytes to recieve from it
Output: the data which got pulled from the given client socket as int
*/
int Communicator::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	unsigned char* buffer = (unsigned char*)getPartFromSocket(sc, bytesNum);

	unsigned char* msg = new unsigned char(bytesNum);
	size_t i;

	for (i = 0; i < bytesNum; ++i)
		msg[bytesNum - 1 - i] = buffer[i];

	int result = 0;
	for (int i = 0, j = sizeof(int); i < bytesNum; i++, j--)
	{
		result += (msg[i] << (i * 8));
	}

	delete buffer;

	return result;
}


/*
The function gets a part from the given socket according to the given bytes num and converts it to string
Input: the client socket to the recive data from and the number of bytes to recieve from it
Output: the data which got pulled from the given client socket as string
*/
std::string Communicator::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* buffer = getPartFromSocket(sc, bytesNum);

	std::string res;
	res.append(buffer, bytesNum);

	delete buffer;

	return res;
}


/*
The function handles the requests from the clients and createas for each client a thread
*/
void Communicator::startHandleRequests()
{
	while (true)
	{
		try
		{
			std::cout << "Waiting for client connection request..." << std::endl;
			
			//this accepts the client and create a specific socket from server to this client
			SOCKET clientSocket = ::accept(m_serverSocket, NULL, NULL);

			if (clientSocket == INVALID_SOCKET)
				throw std::exception(__FUNCTION__ " - accept client");

			m_clients[clientSocket] = nullptr;  //adds the accepted client to the clients map

			//the function that handle the conversation with the client gets activated as a thread
			std::thread currClientHandler(&Communicator::handleNewClient, this, clientSocket);
			currClientHandler.detach();
		}
		catch (std::exception& e)
		{
			std::cout << "Error occured: " << e.what() << std::endl;
		}
	}
}


/*
The function creates the server
*/
void Communicator::bindAndListen()
{
	//creates a server socket on IP and TCP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(SERVER_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << SERVER_PORT << std::endl;
}


/*
The function handles every client and his request
*/
void Communicator::handleNewClient(SOCKET socket)
{
	//sends the first message to the client and waits for his request
	this->sendData(socket, "hello");
	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//gets the whole message from the socket
	int msgCode = this->getIntPartFromSocket(socket, CODE_BYTE_LENGTH);
	int dataLength = this->getIntPartFromSocket(socket, DATA_BYTE_LENGTH);
	std::string data = this->getStringPartFromSocket(socket, dataLength);

	LoginRequestHandler* handler = m_handlerFactory.createLoginRequestHandler();

	//creates the reqeust info struct to send to the loginreqeuesthandler
	RequestInfo request;
	request.buffer = data;
	request.id = RequestMessageCode(msgCode);
	
	RequestResult result;
	try
	{
		//creates the request result struct
		result = handler->handleRequest(request);
	}
	catch (std::exception& e)  //incase of an error that occures, the server needs to send to the client a fitting message
	{
		//creates the error response struct
		ErrorResponse responseStruct;
		responseStruct.error = e.what();
		Buffer responseBuffer = JsonResponsePacketSerializer::serializeResponse(responseStruct);

		result.response = responseBuffer;
		result.newHandler = handler;
	}

	//sends the result to the client
	this->sendData(socket, result.response);
}

