#pragma once

#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginManager.h"


class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager& loginManager);
	~LoginRequestHandler() = default;

	virtual bool isRequestRelevant(RequestInfo info);
	virtual RequestResult handleRequest(RequestInfo info);

private:
	//variables
	LoginManager& m_loginManager; 

	//functions
	RequestResult login(RequestInfo info);
	RequestResult signup(RequestInfo info);

};

