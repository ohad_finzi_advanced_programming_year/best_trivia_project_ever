#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include <io.h>
#include <sstream>

#define QUESTIONS_SITE_URL "https://opentdb.com/api.php?amount=1&type=multiple"

#define NAME_REPLACE "NAME_REPLACE"
#define PASSWORD_REPLACE "PASSWORD_REPLACE"
#define EMAIL_REPLACE "EMAIL_REPLACE"
#define ID_REPLACE "ID_REPLACE"
#define QUESTION_REPLACE "QUESTION_REPLACE"
#define LIMIT_REPLACE "LIMIT_REPLACE"

#define INCORRECT_ANSWERS "incorrect_answers"
#define CORRECT_ANSWER "correct_answer"
#define QUESTION "question"

#define NUM_TO_READ 250
#define NUM_OF_QUESTIONS 30
#define NUM_OF_INCORRECT_ANSWERS 3


class SqliteDataBase: public IDatabase
{
	sqlite3* _db;
	bool open();
	bool insertQuestionsIntoDB();
	static int doesExistCallback(void* data, int argc, char** argv, char** azColName);
	static int averageAnswerTimeCallback(void* data, int argc, char** argv, char** azColName);
	static int getUserColomnDataCallback(void* data, int argc, char** argv, char** azColName);
	static int getQuestionsCallback(void* data, int argc, char** argv, char** azColName);

public:
	SqliteDataBase();
	~SqliteDataBase();

	//users table functions
	virtual bool doesUserExist(std::string usernameToSearch) override;
	virtual bool doesPasswordMatch(std::string username, std::string passwordToCheck) override;
	virtual void addNewUser(std::string username, std::string password, std::string email) override;

	//statistics table functions
	virtual float getPlayerAverageAnswerTime(std::string playerName) override;
	virtual int getNumOfCorrectAnswers(std::string playerName) override;
	virtual int getNumOfTotalAnswers(std::string playerName) override;
	virtual int getNumOfPlayerGames(std::string playerName) override;

	//questions table functions
	virtual std::list<Question> getQuestions(int numOfQuestions) override;
};

