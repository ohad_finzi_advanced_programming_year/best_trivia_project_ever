#include "LoginManager.h"

//cto'r
LoginManager::LoginManager(IDatabase* database):
	m_database(database), m_loggedUsers()
{}


/*
The function signs up a new user to the trivia app with the given parameters
Input: username of the new user, password and his email
Output: None
*/
void LoginManager::signup(std::string username, std::string password, std::string email)
{
	//checks if the given username exists
	if (m_database->doesUserExist(username))
	{
		throw std::exception("User already exists!");
	}

	m_database->addNewUser(username, password, email);
}


/*
The function logs a user into the trivia app using the given parameters
Input: username and password to log into the trivia app
Output: None
*/
void LoginManager::login(std::string username, std::string password)
{
	//checks if the given username exists
	if (!m_database->doesUserExist(username))
	{
		throw std::exception("User doesn't exists!");
	}

	//if the password matches the username adds it to the vector of connected users
	if (!m_database->doesPasswordMatch(username, password))
	{
		throw std::exception("Wrong password!");
	}

	//checks if the given user already logged in
	for (auto currUser : m_loggedUsers)
	{
		if (currUser.getUsername() == username)
		{
			throw std::exception("User already logged in!");
		}
	}

	m_loggedUsers.push_back(LoggedUser(username));
}


/*
The function logs out a given username from the trivia app
Input: username to logout
Output: None
*/
void LoginManager::logout(std::string username)
{
	//goes over each element in the vector and if its username is the same as the parameter - removes it from the vector of connected users
	for (std::vector<LoggedUser>::iterator itrToRemove = m_loggedUsers.begin(); itrToRemove != m_loggedUsers.end(); ++itrToRemove)
	{
		//if the current user matches the given username - removing the user from the vector
		if (itrToRemove->getUsername() == username)
		{
			m_loggedUsers.erase(itrToRemove);
			break;
		}
	}
}