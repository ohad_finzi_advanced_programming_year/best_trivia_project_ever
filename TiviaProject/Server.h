#pragma once

#include "RequestHandlerFactory.h"
#include "Communicator.h"
#include "SqliteDataBase.h"

#include <iostream>
#include <string>
#include <thread>

#define EXIT_MSG "EXIT"


class Server
{
public:
	Server();
	~Server();

	void run();

private:
	IDatabase* m_database;
	RequestHandlerFactory m_handlerFactory;
	Communicator m_communicator;

};

