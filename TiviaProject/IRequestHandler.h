#pragma once

#include <iostream>
#include <string>
#include <ctime>

typedef std::string Buffer;


class IRequestHandler;

enum RequestMessageCode
{
	LoginRequestCode = 201,
	SignupRequestCode = 202,

	LogoutRequestCode = 203,
	GetRoomsRequestCode = 204,
	GetPlayersInRoomRequestCode = 205,
	JoinRoomRequestCode = 206,
	CreateRoomRequestCode = 207,
	GetStatisticsRequestCode = 208
};

struct RequestInfo
{
	RequestMessageCode id;
	std::time_t receivalTime;
	Buffer buffer;
};

struct RequestResult
{
	Buffer response;
	IRequestHandler* newHandler;
};

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};


struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};


struct JoinRoomRequest
{
	unsigned int roomId;
};


struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};


class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo info) = 0;
	virtual RequestResult handleRequest(RequestInfo info) = 0;

protected:
	RequestResult createRequestResult(Buffer responseBuffer, IRequestHandler* handler);

};

