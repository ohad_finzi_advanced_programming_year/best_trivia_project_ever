#pragma once

#include <WinSock2.h>
#include <Windows.h>

#include <iostream>
#include <string>
#include <map>
#include <thread>

#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"

#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"


#define SERVER_PORT 9092

#define CODE_BYTE_LENGTH 1
#define DATA_BYTE_LENGTH 4


class Communicator
{
public:
	Communicator(RequestHandlerFactory& handlerFactory);
	~Communicator() = default;

	void startHandleRequests();

private:
	//variables
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handlerFactory;

	void bindAndListen();
	void handleNewClient(SOCKET socket);

	void sendData(SOCKET socket, std::string data);

	//receive messages from socket
	char* getPartFromSocket(SOCKET socket, int bytesNum);
	int getIntPartFromSocket(SOCKET socket, int bytesNum);
	std::string getStringPartFromSocket(SOCKET socket, int bytesNum);

};

