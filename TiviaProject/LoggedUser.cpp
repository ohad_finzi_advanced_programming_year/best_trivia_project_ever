#include "LoggedUser.h"

//cto'r
LoggedUser::LoggedUser(std::string username) :
	m_username(username)
{}


/*
The function returns a string which contains the username
Input: None
Output: the username
*/
std::string LoggedUser::getUsername()
{
	return m_username;
}