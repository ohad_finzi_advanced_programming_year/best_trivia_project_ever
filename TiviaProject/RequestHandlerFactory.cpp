#include "RequestHandlerFactory.h"


//cto'r
RequestHandlerFactory::RequestHandlerFactory(IDatabase* database):
	m_database(database), m_loginManager(m_database)
{
}


/*
The function creates a new login request handler and returns it as a pointer
*/
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(m_loginManager);
}


/*
The function returns the login manager object of the factory handler class
*/
LoginManager& RequestHandlerFactory::getLoginManager()
{
	return m_loginManager;
}

