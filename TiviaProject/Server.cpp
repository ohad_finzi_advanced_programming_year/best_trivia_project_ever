#include "Server.h"


//cto'r
Server::Server():
	m_database(new SqliteDataBase()), m_handlerFactory(m_database), m_communicator(m_handlerFactory)
{
}


//dto'r
Server::~Server()
{
	delete m_database;
}


/*
The function activates the Communicator class as the server of this project
*/
void Server::run()
{
	std::string input;

	//activates the server as a thread
	std::thread serverHandler(&Communicator::startHandleRequests, &m_communicator);
	serverHandler.detach();

	//a loop which runs until the user which controls the server types "EXIT"
	do
	{
		std::cin >> input;
	} 	
	while (input != EXIT_MSG);

	exit(-1);
}

