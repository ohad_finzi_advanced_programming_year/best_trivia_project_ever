#include "IRequestHandler.h"


/*
The function creates a request result struct using the given information and returns it
Input: the response buffer and the request handler pointer to put inside the request result 
Output: the request result struct filled with the given information
*/
RequestResult IRequestHandler::createRequestResult(Buffer responseBuffer, IRequestHandler* handler)
{
	RequestResult result;
	result.response = responseBuffer;
	result.newHandler = handler;

	return result;
}

