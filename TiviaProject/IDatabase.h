#pragma once

#include "Question.h"
#include <string>
#include <list>

class IDatabase
{
public:
	virtual bool doesUserExist(std::string usernameToSearch) = 0;
	virtual bool doesPasswordMatch(std::string username, std::string passwordToCheck) = 0;
	virtual void addNewUser(std::string username, std::string password, std::string email) = 0;
	virtual float getPlayerAverageAnswerTime(std::string playerName) = 0;
	virtual int getNumOfCorrectAnswers(std::string playerName) = 0;
	virtual int getNumOfTotalAnswers(std::string playerName) = 0;
	virtual int getNumOfPlayerGames(std::string playerName) = 0;
	virtual std::list<Question> getQuestions(int numOfQuestions) = 0;
};

