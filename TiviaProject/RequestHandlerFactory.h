#pragma once 

#include "LoginRequestHandler.h"
#include "SqliteDataBase.h"  
#include "LoginManager.h"  


class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* database);
	~RequestHandlerFactory() = default;

	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();  

private:
	IDatabase* m_database;
	LoginManager m_loginManager; 

};

