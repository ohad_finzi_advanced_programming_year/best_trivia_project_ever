#include "RoomManager.h"


/*
The function creates a new room using the given roomdata and adds the given user to it
*/
void RoomManager::createRoom(LoggedUser user, RoomData room)
{
	Room newRoom = Room(room);
	newRoom.addUser(user);

	m_rooms[room.id] = newRoom;
}


/*
The function deletes the room using the given room id
*/
void RoomManager::deleteRoom(unsigned int ID)
{
	//checks if the given id is valid(inside the map of rooms of the current class)
	if (m_rooms.find(ID) == m_rooms.end())
	{
		std::cout << "The room was not found!" << std::endl;
	}

	m_rooms.erase(ID);
}


/*
The function returns the givne room id active state
*/
unsigned int RoomManager::getRoomState(unsigned int ID)
{
	//checks if the given id is valid(inside the map of rooms of the current class)
	if (m_rooms.find(ID) == m_rooms.end())
	{
		std::cout << "The room was not found!" << std::endl;
	}

	return m_rooms[ID].getRoomData().isActive;
}


/*
The function converts the map of all the rooms to a vector with all the rooms data that are active
*/
std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> allRooms;
	RoomData currRoomData;

	//runs through the rooms map
	for (std::map<unsigned int, Room>::iterator currRoom = m_rooms.begin(); currRoom != m_rooms.end(); ++currRoom)
	{
		//adds the current room data if its active
		currRoomData = currRoom->second.getRoomData();
		if (currRoomData.isActive) 
		{
			allRooms.push_back(currRoomData);
		}
	}

	return allRooms;
}

