#pragma once

#include "json.hpp"
using Json = nlohmann::json;

#include "IRequestHandler.h"

#include <iostream>
#include <string>
#include <vector>


class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(Buffer request);
	static SignupRequest deserializeSignupRequest(Buffer request);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(Buffer request);
	static JoinRoomRequest deserializeJoinRoomRequest(Buffer request);
	static CreateRoomRequest deserializeCreateRoomRequest(Buffer request);

};

