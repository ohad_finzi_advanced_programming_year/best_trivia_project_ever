#pragma comment(lib, "urlmon.lib")
#include "SqliteDataBase.h"
#include <urlmon.h>
#include <cstdio>
#include "json.hpp"
using Json = nlohmann::json;

//cto'r
SqliteDataBase::SqliteDataBase() :
	_db()
{
	if (!this->open())
	{
		throw std::exception("Failed to load or open the database!");
	}
}


//dto'r
SqliteDataBase::~SqliteDataBase()
{
	sqlite3_close(_db);
	_db = nullptr;
}


//callback function that when called we know the searched value was found so we change the value of the first parameter to 1
int SqliteDataBase::doesExistCallback(void* data, int argc, char** argv, char** azColName)
{
	//doesExist pointing to the same address of the (int*) parameter data
	int* doesExist = (int*)data;
	*doesExist = 1;
	return 0;
}


//callback function that gets the statistics of a user, and returns his average answer time
int SqliteDataBase::averageAnswerTimeCallback(void* data, int argc, char** argv, char** azColName)
{
	float totalAnswerTime = 0;
	float totalAnswers = 0;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "TOTAL_ANSWER_TIME") {
			totalAnswerTime = atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "TOTAL_ANSWERS") {
			totalAnswers = atoi(argv[i]);
		}
	}
	float* averageAnswerTime = (float*)data;
	*averageAnswerTime = totalAnswerTime / totalAnswers;
	return 0;
}


//callback function that gets the data of a user's colomn, and returns it
int SqliteDataBase::getUserColomnDataCallback(void* data, int argc, char** argv, char** azColName)
{
	//userColomnData pointing to the same address of the (int*) parameter data
	int* userColomnData = (int*)data;
	*userColomnData = atoi(argv[0]);
	return 0;
}


//callback function that inserts the given questions from the server into list of Question objects
int SqliteDataBase::getQuestionsCallback(void* data, int argc, char** argv, char** azColName)
{
	//questions pointing to the same address of the (std::list<Question>*) parameter data
	std::list<Question>* questions = (std::list<Question>*)data;

	//going over all the questions that were returned from the server and inserting them into the questions list
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "QUESTION_INFO") {
			questions->push_back(Question((std::string)argv[i]));
		}
	}
	return 0;
}


/*
The function opens an sqlite database file if already existing or creates a new data base with Users and Questions tables
Input: None
Output: false if failed creating or opening the db, true if the db was opened or created successfully
*/
bool SqliteDataBase::open()
{
	std::string dbFileName = "TriviaDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &_db);
	if (res != SQLITE_OK)//check if the db file was opened successfuly
	{
		_db = nullptr;
		return false;
	}

	//Deleting the data from the table questions if already exists in order to inesrt new questions afterwards
	const char* sqlDeleteQuestions = "DELETE FROM QUESTIONS;";
	char** errMessage = nullptr;
	res = sqlite3_exec(_db, sqlDeleteQuestions, nullptr, nullptr, errMessage);


	//Creating USERS table
	const char* sqlCreateUsers = "CREATE TABLE IF NOT EXISTS USERS(\
		NAME TEXT PRIMARY KEY NOT NULL, \
		PASSWORD TEXT NOT NULL, \
		EMAIL TEXT NOT NULL\
		);";

	res = sqlite3_exec(_db, sqlCreateUsers, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)//Checking if creation of the table USERS was successfull
	{
		return false;
	}

	//Creating QUESTIONS table
	const char* sqlCreateQuestions = "CREATE TABLE IF NOT EXISTS QUESTIONS(\
		ID INTEGER PRIMARY KEY, \
		QUESTION_INFO TEXT NOT NULL\
		);";

	res = sqlite3_exec(_db, sqlCreateQuestions, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)//Checking if creation of the table QUESTIONS was successfull
	{
		return false;
	}
	this->insertQuestionsIntoDB();
	
	//Creating STATISTICS table
	const char* sqlCreateStatistics = "CREATE TABLE IF NOT EXISTS STATISTICS(\
		USERNAME TEXT PRIMARY KEY NOT NULL, \
		GAMES_PLAYED INTEGER, \
		TOTAL_ANSWERS INTEGER, \
		CORRECT_ANSWERS INTEGER, \
		TOTAL_ANSWER_TIME INTEGER, \
		CONSTRAINT FK_userStatistic FOREIGN KEY(USERNAME)\
		REFERENCES USERS(NAME)\
		);";

	res = sqlite3_exec(_db, sqlCreateStatistics, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)//Checking if creation of the table STATISTICS was successfull
	{
		return false;
	}

	return true;
}


/*
The functions inserts questions into the datbase questions table from a site that returns random questions in json format
Input: None
Output: None
*/
bool SqliteDataBase::insertQuestionsIntoDB()
{
	// Windows IStream interface
	IStream* stream;
	
	//Getting 30 questions into the questions table from the url which returns questions in json format
	for (int i = 0; i < NUM_OF_QUESTIONS; i++)
	{
		// we will get the input from the url stream into the buffer
		char buff[256];

		std::string questionInfoJson;

		unsigned long bytesRead;

		// make a call to the URL - a non-zero return means some error
		if (URLOpenBlockingStreamA(0, QUESTIONS_SITE_URL, &stream, 0, 0))
		{
			throw std::exception("An Error has occured while inserting questions into the db questions table");
		}

		while (true)
		{
			//Reads 250 bytes from the stream object into buff and stores the actual bytes read to "bytesRead"
			stream->Read(buff, NUM_TO_READ, &bytesRead);

			//read all the bytes from the input, ending the while loop
			if (0U == bytesRead)
			{
				break;
			}

			//appends the bytes that were read from buff into the string
			questionInfoJson.append(buff, bytesRead);
		};
		//parsing the json content into a json object
		Json jsonResponse = Json::parse(questionInfoJson);

		//a non-zero return means some error
		if (jsonResponse["response_code"] != 0)
		{
			throw std::exception ("Error has occured");
		}

		//we only need the results, so we ignore the other parts of the json response
		Json fullJsonData = jsonResponse["results"][0];

		//adding the question itself to the string of the question data in the json format
		std::stringstream currentQuestionInfo;
		currentQuestionInfo << "{\"" << QUESTION << "\":" << fullJsonData[QUESTION];

		//adding the correct answer to the string of the question data in the json format
		currentQuestionInfo << ",\"" << CORRECT_ANSWER << "\":" << fullJsonData[CORRECT_ANSWER];

		//adding the three incorrect answers to the string of the question data in the json format
		currentQuestionInfo << ",\"" << INCORRECT_ANSWERS << "\":[";
		std::string comma = "";
		for (int j = 0; j < NUM_OF_INCORRECT_ANSWERS; j++)
		{
			currentQuestionInfo << comma << fullJsonData[INCORRECT_ANSWERS][j];
			comma = ",";
		}
		//adding ']}' to close the json format 
		currentQuestionInfo << "]}";

		std::string sqlAddQuestion = "INSERT INTO QUESTIONS (ID, QUESTION_INFO) VALUES (ID_REPLACE, 'QUESTION_REPLACE');";
		std::string idReplace = ID_REPLACE;
		std::string questionReplace = QUESTION_REPLACE;

		//inserting the question id and the question info in json format into the sql request
		sqlAddQuestion.replace(sqlAddQuestion.find(ID_REPLACE), idReplace.length(), std::to_string(i+1));
		sqlAddQuestion.replace(sqlAddQuestion.find(QUESTION_REPLACE), questionReplace.length(), currentQuestionInfo.str());

		char** errMessage = nullptr;
		int res = sqlite3_exec(_db, sqlAddQuestion.c_str(), nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)//Checking if insert of question was successfull
		{
			return false;
		}
	}

	// release the interface
	// good programming practice
	stream->Release();
}


/*
The function checks if a certain user already exists in the database
Input: name of the user to search for
Output: true if the user exists, else false
*/
bool SqliteDataBase::doesUserExist(std::string usernameToSearch)
{
	int doesExist = NULL;
	std::string sqlDoesUserExist = "SELECT * FROM USERS WHERE NAME = 'NAME_REPLACE';";
	std::string nameReplace = NAME_REPLACE;

	//inserting the username to be searched for, into the sql request
	sqlDoesUserExist.replace(sqlDoesUserExist.find(NAME_REPLACE), nameReplace.length(), usernameToSearch);

	char** errMessage = nullptr;
	//sending query to the server with callback function and parameter to the function
	int res = sqlite3_exec(_db, sqlDoesUserExist.c_str(), doesExistCallback, &doesExist, errMessage);
	if (doesExist == 1)
	{
		return true;
	}
	return false;
}


/*
The function checks if a given password matches the user
Input: the user's name and password
Output: true if the password matches, else false
*/
bool SqliteDataBase::doesPasswordMatch(std::string username, std::string passwordToCheck)
{
	int doesExist = NULL;
	std::string sqlDoesPasswordMatch = "SELECT * FROM USERS WHERE NAME = 'NAME_REPLACE' AND PASSWORD = 'PASSWORD_REPLACE';";
	std::string nameReplace = NAME_REPLACE;
	std::string passwordReplace = PASSWORD_REPLACE;

	//inserting the values to be searched for, into the sql request
	sqlDoesPasswordMatch.replace(sqlDoesPasswordMatch.find(NAME_REPLACE), nameReplace.length(), username);
	sqlDoesPasswordMatch.replace(sqlDoesPasswordMatch.find(PASSWORD_REPLACE), passwordReplace.length(), passwordToCheck);

	char** errMessage = nullptr;
	//sending query to the server with callback function and parameter to the function
	int res = sqlite3_exec(_db, sqlDoesPasswordMatch.c_str(), doesExistCallback, &doesExist, errMessage);
	if (doesExist == 1)
	{
		return true;
	}
	return false;
}


/*
The funcion adds a new user to the users table in the db
Input: The user's name, password and email
Output: None
*/
void SqliteDataBase::addNewUser(std::string username, std::string password, std::string email)
{
	std::string sqlAddUser = "INSERT INTO USERS (NAME, PASSWORD, EMAIL) VALUES ('NAME_REPLACE', 'PASSWORD_REPLACE', 'EMAIL_REPLACE');";
	std::string nameReplace = NAME_REPLACE;
	std::string passwordReplace = PASSWORD_REPLACE;
	std::string emailReplace = EMAIL_REPLACE;

	//inserting the relevant values of the new user into the sql request
	sqlAddUser.replace(sqlAddUser.find(NAME_REPLACE), nameReplace.length(), username);
	sqlAddUser.replace(sqlAddUser.find(PASSWORD_REPLACE), passwordReplace.length(), password);
	sqlAddUser.replace(sqlAddUser.find(EMAIL_REPLACE), emailReplace.length(), email);

	char** errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlAddUser.c_str(), nullptr, nullptr, errMessage);
}


/*
The function the average time took a given player to answer each question
Input: The player name
Output: The average time took the player to answer each question
*/
float SqliteDataBase::getPlayerAverageAnswerTime(std::string playerName)
{
	float averageAnswerTime = NULL;
	std::string sqlGetPlayerAverageAnswerTime = "SELECT * FROM STATISTICS WHERE USERNAME = 'NAME_REPLACE';";
	std::string nameReplace = NAME_REPLACE;

	//inserting the username to be searched for, into the sql request
	sqlGetPlayerAverageAnswerTime.replace(sqlGetPlayerAverageAnswerTime.find(NAME_REPLACE), nameReplace.length(), playerName);

	char** errMessage = nullptr;
	//sending query to the server with callback function and parameter to the function
	int res = sqlite3_exec(_db, sqlGetPlayerAverageAnswerTime.c_str(), averageAnswerTimeCallback, &averageAnswerTime, errMessage);
	return averageAnswerTime;
}


/*
The function the number of total correct answers a given player has answered
Input: The player name
Output: The number of total correct answers the player has answered
*/
int SqliteDataBase::getNumOfCorrectAnswers(std::string playerName)
{
	int correctAnswers = NULL;
	std::string sqlGetNumOfCorrectAnswers = "SELECT CORRECT_ANSWERS FROM STATISTICS WHERE USERNAME = 'NAME_REPLACE';";
	std::string nameReplace = NAME_REPLACE;

	//inserting the username to be searched for, into the sql request
	sqlGetNumOfCorrectAnswers.replace(sqlGetNumOfCorrectAnswers.find(NAME_REPLACE), nameReplace.length(), playerName);

	char** errMessage = nullptr;
	//sending query to the server with callback function and parameter to the function
	int res = sqlite3_exec(_db, sqlGetNumOfCorrectAnswers.c_str(), getUserColomnDataCallback, &correctAnswers, errMessage);
	return correctAnswers;
}


/*
The function returns the number of total answers a given player has answered
Input: The player name
Output: The number of total answers the player has answered
*/
int SqliteDataBase::getNumOfTotalAnswers(std::string playerName)
{
	int totalAnswers = NULL;
	std::string sqlGetNumOfTotalAnswers= "SELECT TOTAL_ANSWERS FROM STATISTICS WHERE USERNAME = 'NAME_REPLACE';";
	std::string nameReplace = NAME_REPLACE;

	//inserting the username to be searched for, into the sql request
	sqlGetNumOfTotalAnswers.replace(sqlGetNumOfTotalAnswers.find(NAME_REPLACE), nameReplace.length(), playerName);

	char** errMessage = nullptr;
	//sending query to the server with callback function and parameter to the function
	int res = sqlite3_exec(_db, sqlGetNumOfTotalAnswers.c_str(), getUserColomnDataCallback, &totalAnswers, errMessage);
	return totalAnswers;
}


/*
The function returns the number of games a given player has played
Input: The player name
Output: The number of games the player has played
*/
int SqliteDataBase::getNumOfPlayerGames(std::string playerName)
{
	int gamesPlayed = NULL;
	std::string sqlGetNumOfPlayerGames = "SELECT GAMES_PLAYED FROM STATISTICS WHERE USERNAME = 'NAME_REPLACE';";
	std::string nameReplace = NAME_REPLACE;

	//inserting the username to be searched for, into the sql request
	sqlGetNumOfPlayerGames.replace(sqlGetNumOfPlayerGames.find(NAME_REPLACE), nameReplace.length(), playerName);

	char** errMessage = nullptr;
	//sending query to the server with callback function and parameter to the function
	int res = sqlite3_exec(_db, sqlGetNumOfPlayerGames.c_str(), getUserColomnDataCallback, &gamesPlayed, errMessage);
	return gamesPlayed;
}


/*
The function returns a list of given number of questions from the questions db table
Input: Number of questions to return
Output: List which contains the requested number of questions
*/
std::list<Question> SqliteDataBase::getQuestions(int numOfQuestions)
{
	std::list<Question> retQuestions;
	std::string sqlGetQuestions = "SELECT * FROM QUESTIONS LIMIT 0, LIMIT_REPLACE;";
	std::string limitReplace = LIMIT_REPLACE;

	//inserting the number of questions to be returned into the sql request
	sqlGetQuestions.replace(sqlGetQuestions.find(LIMIT_REPLACE), limitReplace.length(), std::to_string(numOfQuestions));

	char** errMessage = nullptr;
	//sending query to the server with callback function and parameter to the function
	int res = sqlite3_exec(_db, sqlGetQuestions.c_str(), getQuestionsCallback, &retQuestions, errMessage);
	return retQuestions;
}