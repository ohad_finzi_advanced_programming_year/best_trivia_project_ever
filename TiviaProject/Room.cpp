#include "Room.h"


/*
The function converts the struct roomdata to a string
*/
std::string RoomData::toString()
{
	return "RoomId: " + std::to_string(id) + ", RoomName: " + name + ", maxPlayers: " + std::to_string(maxPlayers) + ", numOfQuestions: " + std::to_string(numOfQuestions) + ", timePerQuestion: " + std::to_string(timePerQuestion) + ", isActive: " + std::to_string(isActive) + ". ";
}


//cto'r
Room::Room(RoomData data):
	m_metadata(data)
{
}


/*
The funciton adds the given user to the user's life of the current Room class if its not already in it
*/
void Room::addUser(LoggedUser userToAdd)
{
	//cant add a user if the room is active and if the room is full
	if (m_metadata.isActive || m_metadata.maxPlayers == m_users.size())
	{
		std::cout << "Can't add user to the current room!" << std::endl;
		return;
	}

	//loops over the logged users list to search for the given user. if the given user isnt in the users list it will get added
	for (std::vector<LoggedUser>::iterator currUser = m_users.begin(); currUser != m_users.end(); ++currUser)
	{
		if (currUser->getUsername() == userToAdd.getUsername())
		{
			std::cout << "User already in the current room!" << std::endl;
			return;
		}
	}

	m_users.push_back(userToAdd);
}


/*
The funciton removes the given user from the user's list of the current Room class
*/
void Room::removeUser(LoggedUser userToRemove)
{
	//loops over the logged users list to remove the given user
	for (std::vector<LoggedUser>::iterator currUser = m_users.begin(); currUser != m_users.end(); ++currUser)
	{
		if (currUser->getUsername() == userToRemove.getUsername())  //if the current user is the given user it will get deleted from the users list
		{
			m_users.erase(currUser);
			return;
		}
	}
}


/*
The function returns the current room data as a struct
*/
RoomData Room::getRoomData()
{
	return m_metadata;
}


/*
The function gets all the users name from the curret Room class and returns it
*/
std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> allUsers;

	//loops over the logged users list and adds each user's name to a name list and returns it
	for (std::vector<LoggedUser>::iterator currUser = m_users.begin(); currUser != m_users.end(); ++currUser)
	{
		allUsers.push_back(currUser->getUsername());
	}

	return allUsers;
}

