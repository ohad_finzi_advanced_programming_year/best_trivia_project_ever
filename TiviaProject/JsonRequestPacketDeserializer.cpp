#include "JsonRequestPacketDeserializer.h"


/*
The function deserializes a login request from bytes into readable data
Input: Buffer which contains the data of the login requests as bytes
Output:	LoginRequest struct that has the content of the request as readable data
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(Buffer request)
{
	Json jsonRequest;
	jsonRequest = Json::parse(request);

	LoginRequest login;
	login.username = jsonRequest["Username"];
	login.password = jsonRequest["Password"];

	return login;
}


/*
The function deserializes a signup request from bytes into readable data
Input: Buffer which contains the data of the signup requests as bytes
Output:	SignupRequest struct that has the content of the request as readable data
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(Buffer request)
{
	Json jsonRequest;
	jsonRequest = Json::parse(request);

	SignupRequest signup;
	signup.username = jsonRequest["Username"];
	signup.password = jsonRequest["Password"];
	signup.email = jsonRequest["Email"];

	return signup;
}


/*
The function deserializes a getplayers request from bytes into readable data
Input: Buffer which contains the data of the getplayers request as bytes
Output: GetPlayersInRoomRequest struct that has the content of the request as readable data
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(Buffer request)
{
	Json jsonRequest;
	jsonRequest = Json::parse(request);

	GetPlayersInRoomRequest getPlayersInRoom;
	getPlayersInRoom.roomId = jsonRequest["RoomId"];

	return getPlayersInRoom;
}


/*
The function deserializes a joinroom request from bytes into readable data
Input: Buffer which contains the data of the getplayers request as bytes
Output: JoinRoomRequest struct that has the content of the request as readable data
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(Buffer request)
{
	Json jsonRequest;
	jsonRequest = Json::parse(request);

	JoinRoomRequest joinRoom;
	joinRoom.roomId = jsonRequest["RoomId"];

	return joinRoom;
}


/*
The function deserializes a createroom request from bytes into readable data
Input: Buffer which contains the data of the getplayers request as bytes
Output: CreateRoomRequest struct that has the content of the request as readable data
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(Buffer request)
{
	Json jsonRequest;
	jsonRequest = Json::parse(request);

	CreateRoomRequest createRoom;
	createRoom.roomName = jsonRequest["RoomName"];
	createRoom.maxUsers = jsonRequest["MaxUsers"];
	createRoom.questionCount = jsonRequest["QuestionCount"];
	createRoom.answerTimeout = jsonRequest["AnswerTimeout"];

	return createRoom;
}


