#pragma once

#include <iostream>
#include <map>

#include "Room.h"


class RoomManager
{
public:
	//functions
	void createRoom(LoggedUser user, RoomData room);
	void deleteRoom(unsigned int ID);
	unsigned int getRoomState(unsigned int ID);
	std::vector<RoomData> getRooms();

private:
	//variables
	std::map<unsigned int, Room> m_rooms;

};

