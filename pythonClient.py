import socket
import json

SERVER_PORT = 9092
SERVER_IP = "127.0.0.1"
ERROR_MSG = "Error occured while sending your message: "
HELLO_MSG = "hello"
USERNAME = "username"
PASSWORD = "password"
MAIL = "email"
LOGIN_MSG_CODE = 201
SIGNUP_MSG_CODE = 202


def connect_to_server():
    """
    The function creates a connection to the remote server
    Input: None
    Output: the client's socket
    """
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    remote_server_address = (SERVER_IP, SERVER_PORT)
    try:
        client_sock.connect(remote_server_address)
    except Exception as exception:
        print("Connection Error:", exception)
        return True

    return client_sock


def get_message(client_sock):
    """
    The function gets a message from the server and prints it to the client
    Input: the client's socket
    Output: None
    """
    try:
        #Recieving message from remote server
        server_msg = client_sock.recv(1024)
        decoded_msg = server_msg.decode()
        print(decoded_msg)
        return decoded_msg
    except Exception as exception:
        print("Error occured while reciving message from the server: ", exception)


def send_request(client_sock, request_message):
    """
    The function sends request message to the server
    Input: the client's socket, the request message in the protocol field (str)
    Output: None
    """
    try:
        # Sending request to server
        client_sock.sendall(request_message.encode())
    except Exception as exception:
        print(ERROR_MSG, exception)


def login_or_signup():
    """
    The function gets the user choice in which way to enter the trivia game, signup or login
    Input: None
    Output: true if the user chose to login, else false
    """

    #performing a do-while loop until the user inserts the wanted input 'l' or 's'
    while True:
        choice = input("Please enter log in order ot login, or sign to signup: ")
        if choice == 'log' or choice == 'sign':
            break

    if choice == 'log':
        return True
    return False


def send_signup_and_login_request(client_sock, is_login):
    """
    The function sends a login request if its second parameter is true, else sends a signup message to the server
    Input: the client's socket
    Output: None
    """
    json_msg_dict = {}
    json_msg_dict[USERNAME] = input("Please enter username: ")
    json_msg_dict[PASSWORD] = input("Please enter password: ")

    if not is_login:
        json_msg_dict[MAIL] = input("Please enter mail: ")

    json_object_to_send = json.dumps(json_msg_dict)

    #converting the length of the json object to 4 bytes
    len_array = [chr(len(json_object_to_send) >> i & 0xff) for i in (24,16,8,0)]
    if is_login:
        # adding the the default message code and the message length to the login message json object
        json_object_to_send = chr(LOGIN_MSG_CODE) + ''.join(len_array) + json_object_to_send
    else:
        # adding the the default message code and the message length to the signup message json object
        json_object_to_send = chr(SIGNUP_MSG_CODE) + ''.join(len_array) + json_object_to_send

    try:
        # Sending the request to the remote server
        client_sock.sendall(json_object_to_send.encode("latin-1"))

        # printing the response from the remote server
        get_message(client_sock)
    except Exception as exception:
        print(ERROR_MSG, exception)


def close_socket(socket_to_close):
    """
    Closing given socket
    Input: a given socket
    Input Type: socket
    Output: None
    """
    try:
        socket_to_close.close()
        print("\n\nBye Bye")
    except Exception as exception:
        print("Error occured while closing the wanted socket", exception)



def main():

    client_sock = connect_to_server()
    server_msg = get_message(client_sock)
    if server_msg == HELLO_MSG:
        entry_choice = login_or_signup()
        send_signup_and_login_request(client_sock, entry_choice)



    close_socket(client_sock)


if __name__ == "__main__":
    main()